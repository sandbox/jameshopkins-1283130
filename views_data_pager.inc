<?php

class views_data_pager extends views_plugin_pager {

  function summary_title() {
    return format_plural($this->options['items_per_page'], '@count item per page', '@count items per page', array('@count' => $this->options['items_per_page']));
  }
	
  // We don't to return a themed pager, as pagination is directly
  // controlled by GET query strings
  function use_pager() {
    return FALSE;
  }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['items_per_page'] = array('default' => 5);
    return $options;
  }
  
  function options_form(&$form, &$form_state) {
  	$pager_text = $this->display->handler->get_pager_text();
  	$form['items_per_page'] = array(
      '#title' => $pager_text['items per page title'],
      '#type' => 'textfield',
      '#description' => $pager_text['items per page description'],
      '#default_value' => $this->options['items_per_page'],
    );
  }
  
  function query() {
  	
    // If the query string doesn't exist, then default to 0.
    $offset = (is_int($_GET['offset'])) ? $_GET['offset'] : 0;

    // If the query string doesn't exist, then use the value defined by the user in the settings form
    $limit = (is_int($_GET['limit'])) ? $_GET['limit'] : $this->options['items_per_page'];
    
    $this->view->query->set_limit($limit);
    $this->view->query->set_offset($offset);
    
  }
  
}
