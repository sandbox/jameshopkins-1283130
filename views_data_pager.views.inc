<?php

function views_data_pager_views_plugins() {

  return array(
    'pager' => array(
      'services' => array(
        'title' => t('Data pager'),
        'help' => t('Expose conventional Views pager parameters as URI query strings to allow dynamic \'subsets\' of results to be returned'),
        'handler' => 'views_data_pager',
        'uses options' => TRUE,
      ),
    ),
  );
}