### VIEWS DATA PAGER ###

By extending the built-in Views pager plugin, Views Data Pager exposes conventional pager variables ($limit and $offset) as URL query strings, allowing 'pagination' to be controlled by these querystrings, hence, no physical pager need be used.

This module is designed to be used in conjunction with Views that are used to return data feeds.

--- INSTALLATION INSTRUCTIONS ---

1. Install the Views Data Pager module at /admin/modules
2. Construct/edit a View
3. Edit the Pager settings, and select 'Data pager'.
4. You can set the default 'items per page' value in the Pager settings itself.
5. Save these settings and display the View in the context of your data feed.